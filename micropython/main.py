from machine import Pin, PWM
from time import sleep

buzzer = PWM(Pin(17))

key_A3S = Pin(0, Pin.IN, Pin.PULL_UP)
key_B3 = Pin(1, Pin.IN, Pin.PULL_UP)
key_C4 = Pin(2, Pin.IN, Pin.PULL_UP)
key_C4S = Pin(3, Pin.IN, Pin.PULL_UP)
key_D4 = Pin(4, Pin.IN, Pin.PULL_UP)
key_D4S = Pin(5, Pin.IN, Pin.PULL_UP)
key_E4 = Pin(6, Pin.IN, Pin.PULL_UP)
key_F4 = Pin(7, Pin.IN, Pin.PULL_UP)
key_F4S = Pin(8, Pin.IN, Pin.PULL_UP)
key_G4 = Pin(9, Pin.IN, Pin.PULL_UP)
key_G4S = Pin(10, Pin.IN, Pin.PULL_UP)
key_A4 = Pin(11, Pin.IN, Pin.PULL_UP)
key_A4S = Pin(12, Pin.IN, Pin.PULL_UP)
key_B4 = Pin(13, Pin.IN, Pin.PULL_UP)
key_C5 = Pin(14, Pin.IN, Pin.PULL_UP)
key_C5S = Pin(15, Pin.IN, Pin.PULL_UP)

# Hz
A3S = const(233)
B3 = const(247)
C4 = const(262)
C4S = const(277)
D4 = const(294)
D4S = const(311)
E4 = const(330)
F4 = const(349)
F4S = const(370)
G4 = const(392)
G4S = const(415)
A4 = const(440)
A4S = const(466)
B4 = const(494)
C5 = const(523)
C5S = const(554)

# set duty cycle, range 0-65535
#duty = int(65535/2)
duty = const(32768)

def play_tone(frequency):
    buzzer.duty_u16(duty)
    # Play tone
    buzzer.freq(frequency)

def be_quiet():
    # Set minimum volume
    buzzer.duty_u16(0)

# LAUNCH JINGLE
play_tone(C4)
sleep(0.25)
play_tone(D4)
sleep(0.25)
play_tone(E4)
sleep(0.25)
play_tone(F4)
sleep(0.25)
play_tone(G4)
sleep(0.25)
play_tone(A4)
sleep(0.25)
play_tone(B4)
sleep(0.25)
play_tone(C5)
sleep(0.5)
be_quiet()

while True:
    if (key_A3S.value()==0):
        play_tone(A3S)
    elif (key_B3.value()==0):
        play_tone(B3)
    elif (key_C4.value()==0):
        play_tone(C4)
    elif (key_C4S.value()==0):
        play_tone(C4S)
    elif (key_D4.value()==0):
        play_tone(D4)
    elif (key_D4S.value()==0):
        play_tone(D4S)
    elif (key_E4.value()==0):
        play_tone(E4)
    elif (key_F4.value()==0):
        play_tone(F4)
    elif (key_F4S.value()==0):
        play_tone(F4S)
    elif (key_G4.value()==0):
        play_tone(G4)
    elif (key_G4S.value()==0):
        play_tone(G4S)
    elif (key_B4.value()==0):
        play_tone(B4)
    elif (key_C5.value()==0):
        play_tone(C5)
    elif (key_C5S.value()==0):
        play_tone(C5S)
    else:
        be_quiet()
    sleep(0.01)
            